## Service ##

- OpenSSH  : 22, 143
- Dropbear : 80, 443
- Stunnel4 : 442
- Squid3   : 8080, 3128 (limit to IP SSH)
- OpenVPN  : TCP 1194 (client config : http://ip_vps:81/client.ovpn)
- badvpn   : badvpn-udpgw port 7300
- nginx    : 81

## Script ##

- menu         (Menu)
- usernew      (Create New Account)
- trial        (Create Trial Account)
- remove       (Remove SSH Account)
- check        (Check User Login)
- member       (Check Member SSH)
- delexp       (Delete User expired)
- resvis       (Restart Service Dropbear, Webmin, Squid3, OpenVPN dan SSH)
- reboot       (Reboot VPS)
- speedtest    (Speedtest VPS)
- info         (Display System Information)

## Other features ##

- DDoS Deflate
- Webmin latest 1.881
- Timezone : Asia/Kuala Lumpur (GMT +8)
- IPv6     : [off]

## Installation ##

Copy and paste command under and press Enter. Wait till the process ready.


apt-get update && apt-get install ca-certificates

wget https://bitbucket.org/xtremprivacy/autoscrpz/raw/fa9cf916c872f0661bccf652a7b03b93ac3cf1a0/debian7.sh

chmod +x debian7.sh

./debian7.sh

