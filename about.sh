#!/bin/bash

clear
echo -e "================================================="
echo -e "#       SSH & OpenVPN AutoInstall Script        #" 
echo -e "#-----------------------------------------------#"
echo -e "#          For Debian 7/8 32 & 64 bit           #"
echo -e "#   For VPS with KVM and VMWare virtualization  #"
echo -e "#-----------------------------------------------#"
echo -e "#       Original script by Area 51 Reborn       #"
echo -e "#-----------------------------------------------#"
echo -e "#          Modified by Privacy of PHC           #"
echo -e "================================================="
