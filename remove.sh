#!/bin/bash

read -p "Ssh User Name should be removed : " Users

if getent passwd $Users > /dev/null 2>&1; then
        userdel $Users
        echo -e "User $Users Has been removed."
else
        echo -e "FAILED: User $Users not found."
fi
